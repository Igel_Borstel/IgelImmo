/*
IgelImmo - IgelImmobilien
----

Copyright (c) 2015-2016 Markus Fischer

This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

*/
package de.cag_igs.igelimmo.commands;

import com.evilmidget38.UUIDFetcher;
import de.cag_igs.igelimmo.main.Selection;
import org.bitbucket.igelborstel.igelcore.commandHandlers.PlayerCommand;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;

import de.cag_igs.igelimmo.main.IgelImmo;
import de.cag_igs.igelimmo.main.UserGroup;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

public class RegionControlCommands extends PlayerCommand {
    private IgelImmo plugin;
    private Map<UUID, String> selectedPlots;
    private Map<UUID, Selection> selectionMap;

    public RegionControlCommands(IgelImmo pl) {
        this.plugin = pl;
        selectedPlots = new TreeMap<>();
        selectionMap = new TreeMap<>();
    }

    @Override
    public boolean command(Player player, Command cmd, String[] args) {
        if (cmd.getName().equalsIgnoreCase("regioncontrol")) {
            if (args.length > 0) {
                if (args[0].equalsIgnoreCase("tp") && args.length == 2) {
                    if (player.hasPermission("igelimmobilien.regioncontrol.tp")) {
                        Location tploc = plugin.getDatabaseControl().getTpLocation(args[1]);
                        if (tploc.getWorld().getName().equals("world") && (int) tploc.getX() == 0 && (int) tploc.getY() == 0 && (int) tploc.getZ() == 0) {
                            player.sendMessage("Ungültiger Grundstücksname!");
                        } else {
                            if (plugin.getDatabaseControl().isMember(args[1], player.getUniqueId()) || player.hasPermission("igelimmobilien.regioncontrol.tp.mod")) {
                                player.teleport(tploc);
                                player.sendMessage("Erfolgreich zum Grundstück " + args[1] + " teleportiert!");
                            } else {
                                player.sendMessage("Du bist kein Grundstücksbewohner!");
                            }
                        }
                        return true;
                    }
                }
                if (args[0].equalsIgnoreCase("settp")) {
                    if (player.hasPermission("igelimmobilien.regioncontrol.settp")) {
                        if (plugin.getDatabaseControl().isOwner(this.plugin.getImmobilienManager().getPlotname(player.getLocation()), player.getUniqueId())) {
                            plugin.getDatabaseControl().setTpLocation(this.plugin.getImmobilienManager().getPlotname(player.getLocation()), player.getLocation());
                            player.sendMessage("Teleportlocation gesetzt!");
                            return true;
                        } else {
                            player.sendMessage("Du bist nicht der Owner des Grundstücks!");
                            return true;
                        }
                    }
                }
                if (args[0].equalsIgnoreCase("list") && args.length == 1) {
                    if (player.hasPermission("igelimmobilien.regioncontrol.list")) {
                        for (UserGroup usergroup : plugin.getDatabaseControl().getNameByUUID(player.getUniqueId())) {
                            if (usergroup.isOwner()) {
                                player.sendMessage(ChatColor.RED + "Owner: " + ChatColor.RESET + usergroup.getPlotname());
                            } else if (usergroup.isMember()) {
                                player.sendMessage(ChatColor.GREEN + "Member: " + ChatColor.RESET + usergroup.getPlotname());
                            } else {
                                player.sendMessage("Guest: " + ChatColor.RESET + usergroup.getPlotname());
                            }
                        }
                    }
                    return true;
                } else if (args[0].equalsIgnoreCase("list") && args.length == 2) {
                    if (player.hasPermission("igelimmobilien.regioncontrol.list.mod")) {
                        player.sendMessage("Grundstücke vom Spieler: " + args[1]);
                        UUIDFetcher fetcher = new UUIDFetcher(Arrays.asList(args[1]));
                        Map<String, UUID> response = null;
                        try {
                            response = fetcher.call();
                        } catch (Exception e) {
                            this.plugin.getLogger().warning("Exception while running UUIDFetcher");
                            e.printStackTrace();
                        }
                        if (response != null && (response.get(args[1]) != null)) {
                            for (UserGroup usergroup : plugin.getDatabaseControl().getNameByUUID(response.get(args[1]))) {
                                if (usergroup.isOwner()) {
                                    player.sendMessage(ChatColor.RED + "Owner: " + ChatColor.RESET + usergroup.getPlotname());
                                } else if (usergroup.isMember()) {
                                    player.sendMessage(ChatColor.GREEN + "Member: " + ChatColor.RESET + usergroup.getPlotname());
                                } else {
                                    player.sendMessage("Guest: " + ChatColor.RESET + usergroup.getPlotname());
                                }
                            }
                        }
                    } else {
                        player.sendMessage("Du darfst dir nicht die Grundstücke von anderen Spielern anzeigen lassen");
                    }
                    return true;
                } else if (args[0].equalsIgnoreCase("toggle")) {
                    if (player.hasPermission("igelimmobilien.regioncontrol.toggle")) {
                        player.sendMessage("Du hast den Platziermodus auf " + this.plugin.getDatabaseControl().toggleActive(player.getUniqueId()) + " geändert!");
                    } else {
                        player.sendMessage("Du darfst den Platziermodus nicht umschalten!");
                    }
                    return true;
                } else if (args[0].equalsIgnoreCase("select") && args.length == 2) {
                    //TODO select plot with command argument
                } else if (args[0].equalsIgnoreCase("select") && args.length == 1) {
                    if (player.hasPermission("igelimmobilien.regioncontrol.select")) {
                        String plotname = this.plugin.getImmobilienManager().getPlotname(player.getLocation());
                        if (plotname != "") {
                            selectedPlots.put(player.getUniqueId(), plotname);
                            player.sendMessage(String.format(plugin.getLanguageFile().getConfig().getString("message.commands.rc.select.sucessful"), plotname));
                        } else {
                            player.sendMessage(plugin.getLanguageFile().getConfig().getString("message.commands.rc.select.wrongplot"));
                        }
                    } else {
                        player.sendMessage(plugin.getLanguageFile().getConfig().getString("message.commands.rc.select.forbidden"));
                    }
                    return true;
                } else if (args[0].equalsIgnoreCase("pos1")) {
                    if (player.hasPermission("igelimmobilien.regioncontrol.pos1")) {
                        if (selectionMap.containsKey(player.getUniqueId())) {
                            selectionMap.get(player.getUniqueId()).setPos1(player.getLocation());
                            player.sendMessage(plugin.getLanguageFile().getConfig().getString("message.commands.rc.pos.pos1.sucessful"));
                        } else {
                            selectionMap.put(player.getUniqueId(), new Selection(player.getLocation(), player.getLocation()));
                        }
                    } else {
                        player.sendMessage(plugin.getLanguageFile().getConfig().getString("message.commmands.rc.pos.forbidden"));
                    }
                    return true;
                } else if (args[0].equalsIgnoreCase("pos2")) {
                    if (player.hasPermission("igelimmobilien.regioncontrol.pos2")) {
                        if (selectionMap.containsKey(player.getUniqueId())) {
                            selectionMap.get(player.getUniqueId()).setPos2(player.getLocation());
                            player.sendMessage(plugin.getLanguageFile().getConfig().getString("message.commands.rc.pos.pos2.sucessful"));
                        } else {
                            selectionMap.put(player.getUniqueId(), new Selection(player.getLocation(), player.getLocation()));
                        }
                    } else {
                        player.sendMessage(plugin.getLanguageFile().getConfig().getString("message.commmands.rc.pos.forbidden"));
                    }
                    return true;
                } else if (args[0].equalsIgnoreCase("redefine")) {
                    if (player.hasPermission("igelimmobilien.regioncontrol.redefine")) {
                        if (selectedPlots.containsKey(player.getUniqueId()) && selectionMap.containsKey(player.getUniqueId())) {
                            if (selectionMap.get(player.getUniqueId()).getPos1().equals(selectionMap.get(player.getUniqueId()).getPos2())) {
                                player.sendMessage(plugin.getLanguageFile().getConfig().getString("message.commands.rc.redefine.singleblockselection"));
                                return true;
                            } else {
                                if (plugin.getImmobilienManager().redefineRegion(selectedPlots.get(player.getUniqueId()), selectionMap.get(player.getUniqueId()))) {
                                    player.sendMessage(plugin.getLanguageFile().getConfig().getString("message.commands.rc.redefine.sucessful"));
                                } else {

                                }
                            }
                        } else {
                            player.sendMessage(plugin.getLanguageFile().getConfig().getString("message.commands.rc.redefine.uncompleteselection"));
                        }
                    } else {
                        player.sendMessage(plugin.getLanguageFile().getConfig().getString("message.commands.rc.redefine.forbidden"));
                    }
                    return true;
                }
            }
        }
        return false;
    }

}
