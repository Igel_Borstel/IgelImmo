/*
IgelImmo - IgelImmobilien
----

Copyright (c) 2015-2016 Markus Fischer

This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

*/
package de.cag_igs.igelimmo.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import de.cag_igs.igelimmo.main.IgelImmo;

public class IgelImmobilienCommands implements CommandExecutor {

    private IgelImmo main;

    public IgelImmobilienCommands(IgelImmo m) {
        this.main = m;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String lable, String[] args) {
        if (cmd.getName().equalsIgnoreCase("igelimmobilien") && args.length >= 1) {
            if (args[0].equalsIgnoreCase("version") || args[0].equalsIgnoreCase("ver")) {
                if (sender.hasPermission("igelimmobilien.version")) {
                    sender.sendMessage("[" + main.getDescription().getName() + "] " + String.format(main.getLanguageFile().getConfig().getString("message.commands.ii.version"), main.getDescription().getVersion()));
                    sender.sendMessage("[" + main.getDescription().getName() + "] " + String.format(main.getLanguageFile().getConfig().getString("message.commands.ii.author"), main.getDescription().getAuthors().get(0)));
                    sender.sendMessage("[" + main.getDescription().getName() + "] " + main.getLanguageFile().getConfig().getString("message.commands.ii.helpby"));
                    for (int i = 1; i < main.getDescription().getAuthors().size(); ++i) {
                        sender.sendMessage("[" + main.getDescription().getName() + "] " + main.getDescription().getAuthors().get(i));
                    }
                    return true;
                } else {
                    sender.sendMessage(main.getLanguageFile().getConfig().getString("message.notenoughrights"));
                    return true;
                }
            } else if (args[0].equalsIgnoreCase("reload") || args[0].equalsIgnoreCase("rl")) {
                if (sender.hasPermission("igelimmoblien.reload")) {
                    main.loadConfig();
                    sender.sendMessage("[" + main.getDescription().getName() + "] " + main.getLanguageFile().getConfig().getString("message.commands.ii.reloaded"));
                    return true;
                } else {
                    sender.sendMessage(main.getLanguageFile().getConfig().getString("message.notenoughrights"));
                    return true;
                }
            }
        }
        return false;
    }

}
