/*
IgelImmo - IgelImmobilien
----

Copyright (c) 2015-2016 Markus Fischer

This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

*/
package de.cag_igs.igelimmo.main;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import de.cag_igs.igelimmo.listeners.BlockBreakListener;
import org.bitbucket.igelborstel.igelcore.config.Config;
import org.bukkit.plugin.java.JavaPlugin;


import de.cag_igs.igelimmo.commands.IgelImmobilienCommands;
import de.cag_igs.igelimmo.commands.MemberControlCommands;
import de.cag_igs.igelimmo.commands.RegionControlCommands;
import de.cag_igs.igelimmo.listeners.BlockPlacedListener;
import de.cag_igs.igelimmo.listeners.PlayerLoginListener;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.UUID;

public class IgelImmo extends JavaPlugin {

    private DatabaseControl databasecontrol;
    private ImmobilienManager immomanager;
    private Config languageFile;


    @Override
    public void onEnable() {
        loadConfig();
        databasecontrol = new DatabaseControl(this);
        immomanager = new ImmobilienManager(this);
        languageFile = new Config(this, "language");
        loadLanguageFile();
        this.getServer().getPluginManager().registerEvents(new BlockPlacedListener(this), this);
        this.getServer().getPluginManager().registerEvents(new PlayerLoginListener(this), this);
        this.getServer().getPluginManager().registerEvents(new BlockBreakListener(this), this);
        this.getCommand("igelimmobilien").setExecutor(new IgelImmobilienCommands(this));
        this.getCommand("regioncontrol").setExecutor(new RegionControlCommands(this));
        this.getCommand("membercontrol").setExecutor(new MemberControlCommands(this));
    }

    @Override
    public void onDisable() {
        databasecontrol.close();
    }

    public void loadConfig() {
        this.reloadConfig();
        this.getConfig().options().header("Config fuer IgelImmo 2.0");
        this.getConfig().addDefault("general.world", "World");
        this.getConfig().addDefault("general.maximumplot", 1);
        this.getConfig().addDefault("general.radius", 16);
        this.getConfig().addDefault("material.create", "chest");
        this.getConfig().addDefault("material.border", "fence");
        this.getConfig().addDefault("material.current", "diamond_block");
        this.getConfig().addDefault("material.new", "gold_block");
        this.getConfig().addDefault("protectWorld", true);
        this.getConfig().addDefault("outsidewarning.enabled", false);
        this.getConfig().addDefault("outsidewarning.firstwarning.count", 30);
        this.getConfig().addDefault("outsidewarning.firstwarning.time", 30000);
        this.getConfig().addDefault("outsidewarning.firstwarning.expire", 100000);
        this.getConfig().addDefault("outsidewarning.secondwarning.count", 60);
        this.getConfig().addDefault("outsidewarning.secondwarning.time", 60000);
        this.getConfig().addDefault("outsidewarning.secondwarning.expire", 200000);
        this.getConfig().addDefault("outsidewarning.finalwarning.dividier", 10);
        this.getConfig().addDefault("outsidewarning.finalwarning.time", 10000);
        this.getConfig().addDefault("outsidewarning.finalwarning.expire", 500000);
        this.getConfig().options().copyDefaults(true);
        this.saveConfig();
    }

    public void loadLanguageFile() {
        this.languageFile.reloadConfig();
        this.languageFile.getConfig().addDefault("message.notenoughrights", "Du hast nicht ausreichend Berechtigung dies zu tun!");
        this.languageFile.getConfig().addDefault("message.commands.ii.version", "Version: %1$s");
        this.languageFile.getConfig().addDefault("message.commands.ii.author", "Autor: %1$s");
        this.languageFile.getConfig().addDefault("message.commands.ii.helpby", "Mit Hilfe von: ");
        this.languageFile.getConfig().addDefault("message.commands.ii.reloaded", "wurde neu geladen!");
        this.languageFile.getConfig().addDefault("message.commands.rc.select.forbidden", "Du darfst kein Grundstück für die Bearbeitung selektieren!");
        this.languageFile.getConfig().addDefault("message.commands.rc.select.wrongplot", "Du befindest dich in keinem Grundstück oder in keinem Standardgrundstück.");
        this.languageFile.getConfig().addDefault("message.commands.rc.select.sucessful", "Du hast das Grundstück %1$s erfolgreich ausgewählt.");
        this.languageFile.getConfig().addDefault("message.commmands.rc.pos.forbidden", "Du darfst keine Positionen markieren.");
        this.languageFile.getConfig().addDefault("message.commands.rc.pos.pos1.sucessful", "Position 1 erfolgreich markiert.");
        this.languageFile.getConfig().addDefault("message.commands.rc.pos.pos2.sucessful", "Position 2 erfolgreich markiert.");
        this.languageFile.getConfig().addDefault("message.protection.forbidden.blockbreaking", "Du darfst hier nichts abbauen!");
        this.languageFile.getConfig().addDefault("message.protection.forbidden.blockplacing", "Du darfst hier nicht bauen!");
        this.languageFile.getConfig().addDefault("message.plot.create.sucessfuly", "Du hast dein Grundstück mit dem Namen %1$s erfolgreich erstellt!");
        this.languageFile.getConfig().addDefault("message.plot.create.disabled", "Du hast die Grundstückserstellung ausgeschalten! Gebe /rc toogle ein!");
        this.languageFile.getConfig().addDefault("message.plot.create.toomanyplots", "Du besitzt bereits zu viele Grundstücke. Erstellung von weiteren nicht möglich!");
        this.languageFile.getConfig().addDefault("message.plot.create.notenoughrights", "Du hast nicht genügend Berechtigung ein Grundstück zu erstellen. Wende dich an einen Moderator.");
        this.languageFile.getConfig().addDefault("message.plot.create.overlaps", "Deine Region überschneidet sich mit einer bereits vorhandenen.");
        this.languageFile.getConfig().addDefault("message.plot.change.memberdontexist", "Den Spieler %1$s gibt es nicht");
        this.languageFile.getConfig().addDefault("message.outsidewarning.firstwarning", "Wir haben festgestellt, dass du viele Blöcke außerhalb deines Grundstücks platzierst! Wenn du ein weiteres GS möchtest, wende dich an einen Mod!");
        this.languageFile.getConfig().addDefault("message.outsidewarning.secondwarning", "&2Wir bitten dich darum deine Umgebung nicht zu verschandeln!");
        this.languageFile.getConfig().addDefault("message.outsidewarning.finalwarning", "&4Führe Experimente bitte in der Farmwelt durch!");
        this.languageFile.getConfig().addDefault("message.system.wrongplotsize", "Die Größe für das Grundstück ist falsch angegeben! Wende dich an einen Systemadministrator.");
        this.languageFile.getConfig().addDefault("greetmessage", "Du betrittst das Grundstück von %1$s");
        this.languageFile.getConfig().addDefault("farewellmessage", "Du verlässt das Grundstück von %1$s");
        this.languageFile.getConfig().options().copyDefaults(true);
        this.languageFile.saveConfig();
    }

    public Config getLanguageFile() {
        return this.languageFile;
    }

    public DatabaseControl getDatabaseControl() {
        return databasecontrol;
    }

    public ImmobilienManager getImmobilienManager() {
        return immomanager;
    }

    public String getNameByUUID(UUID uuid) {
        return (this.getServer().getOfflinePlayer(uuid).hasPlayedBefore()) ? this.getServer().getOfflinePlayer(uuid).getName() : "";
    }

}
