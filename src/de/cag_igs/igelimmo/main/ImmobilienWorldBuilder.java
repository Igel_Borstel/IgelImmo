/*
IgelImmo - IgelImmobilien
----

Copyright (c) 2015-2016 Markus Fischer

This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

*/
package de.cag_igs.igelimmo.main;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;

import java.util.ArrayList;

public class ImmobilienWorldBuilder {
    private IgelImmo plugin;
    private static final ArrayList<Material> forbiddenMaterials;

    static {
        forbiddenMaterials = new ArrayList<>();
        forbiddenMaterials.add(Material.AIR);
        forbiddenMaterials.add(Material.YELLOW_FLOWER);
        forbiddenMaterials.add(Material.RED_ROSE);
        forbiddenMaterials.add(Material.LONG_GRASS);
        forbiddenMaterials.add(Material.VINE);
    }

    ImmobilienWorldBuilder(IgelImmo pl) {
        this.plugin = pl;
    }

    public void generate(Location middlePoint, Integer size, Integer half_size, World world) {
        int x = middlePoint.getBlockX();
        int y = middlePoint.getBlockY();
        int z = middlePoint.getBlockZ();
        int height = 1;
        Material material = Material.getMaterial(plugin.getConfig().getString("material.border").toUpperCase());
        Block blockToChange;
        for (int b = 0; b < size.intValue() + 2; b++) {
            for (blockToChange = world.getBlockAt(x - half_size.intValue() - 1, y, (z - half_size.intValue() - 1) + b); !forbiddenMaterials.contains(blockToChange.getType());) {
                blockToChange = world.getBlockAt(x - half_size.intValue() - 1, y + height, (z - half_size.intValue() - 1) + b);
                height++;
            }

            height = 1;
            blockToChange.setType(material);
            for (blockToChange = world.getBlockAt(x + half_size.intValue() + 1, y, (z - half_size.intValue() - 1) + b); !forbiddenMaterials.contains(blockToChange.getType());) {
                blockToChange = world.getBlockAt(x + half_size.intValue() + 1, y + height, (z - half_size.intValue() - 1) + b);
                height++;
            }

            height = 1;
            blockToChange.setType(material);
        }

        for (int b = 0; b < size.intValue(); b++) {
            for (blockToChange = world.getBlockAt((x - half_size.intValue()) + b, y, z + half_size.intValue() + 1); !forbiddenMaterials.contains(blockToChange.getType());) {
                blockToChange = world.getBlockAt((x - half_size.intValue()) + b, y + height, z + half_size.intValue() + 1);
                height++;
            }

            height = 1;
            blockToChange.setType(material);
            for (blockToChange = world.getBlockAt((x - half_size.intValue()) + b, y, z - half_size.intValue() - 1); !forbiddenMaterials.contains(blockToChange.getType()); ) {
                blockToChange = world.getBlockAt((x - half_size.intValue()) + b, y + height, z - half_size.intValue() - 1);
                height++;
            }

            height = 1;
            blockToChange.setType(material);
        }

    }
}
