package de.cag_igs.igelimmo.listeners;

import de.cag_igs.igelimmo.main.IgelImmo;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

/**
 * Created by Markus on 03.09.2016.
 */
public class BlockBreakListener implements Listener {
    private IgelImmo plugin;

    public BlockBreakListener(IgelImmo plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onBlockBreakEvent(BlockBreakEvent event)
    {
        if (event.getBlock().getWorld().getName().equalsIgnoreCase(plugin.getConfig().getString("general.world"))) {
            String plotname = this.plugin.getImmobilienManager().getPlotname(event.getBlock().getLocation());
            if (plotname != null && plotname.isEmpty()) {
                if (plugin.getConfig().getBoolean("protectWorld")) {
                    if (!event.getPlayer().hasPermission("igelimmobilien.util.ignoreWorldProtection")) {
                        event.setCancelled(true);
                        event.getPlayer().sendMessage(plugin.getLanguageFile().getConfig().getString("message.protection.forbidden.blockbreaking"));
                    }
                }
            }
        }
    }
}
